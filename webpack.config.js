var path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const extractSass = new ExtractTextPlugin({
	filename: "yfront.css",
	disable: process.env.NODE_ENV === "development"
});

module.exports = {
	devtool: "source-map",
	watch: true,
	entry: './src/index.js',
	output: {
		path: path.resolve(__dirname, './dist/'),
		filename: 'yfront.js'
	},
	module: {
		loaders: [{
			test: /\.html$/,
			use: [
				// Chained loaders are applied last to first
				{ loader: 'babel-loader' },
				{ loader: 'polymer-webpack-loader' }
			]
		},
		{
			test: /\.js$/,
			loader: 'babel-loader',
			exclude: /node_modules/,
			query: {
				presets: ['es2015']
			}
		},
		{
			test: /yfront\.scss$/,
			loader: extractSass.extract({
				use: [{
					loader: "css-loader"
				}, {
					loader: "sass-loader"
				}],
				// use style-loader in development
				fallback: "style-loader"
			})
		}
		]
	},
	plugins: [
		extractSass
	]
}