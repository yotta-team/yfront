//import '../node_modules/font-awesome/css/font-awesome.css';

import ajax from './js/ajax';
import utils from './js/utils';

// import the CSS we want to use here
import './scss/yfront.scss';

//components
import './components/address-autocomplete.html';
import './components/ajax-button.html';
import './components/input-autocomplete.html';
import './components/list-draggable.html';
import './components/page-selector.html';
import './components/section-selector.html';
import './components/toast-notification.html';
