export class Observable {
    constructor() {
        this._events = {};
    }

    on(eventName, callback) {
        if (!_events[eventName]) {
            _events[eventName] = [];
        }
        _events[eventName].push(callback);
    }

    trigger(eventName, params) {
        if (_events[eventName] && _events[eventName].length) {
            for (var i = 0; i < _events[eventName].length; i++) {
                _events[eventName][i].apply(this, params || []);
            }
        }
    }
};


export class Commons {
    static stripAccents(s) {
        const in_chrs = 'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ',
            out_chrs = 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY',
            chars_rgx = new RegExp('[' + in_chrs + ']', 'g'),
            transl = {},
            lookup = (m) => transl[m] || m;

        for (let i = 0; i < in_chrs.length; i++) {
            transl[in_chrs[i]] = out_chrs[i];
        }

        return s.replace(chars_rgx, lookup);
    }
    static urlFrom(text) {
        return Commons.stripAccents(text.toLowerCase())
            .replace(/\s/g, '-')
            .replace(/-+/g, '-')
            .replace(/\\/g, '/')
            .replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
    }

    static anchorFrom(text) {
        return '#' + Commons.stripAccents(text.toLowerCase())
            .replace(/(\s|\/|\.)/g, '-')
            .replace(/-+/g, '-')
            .replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
    }
}