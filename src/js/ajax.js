export class ajax {
    static request (url, method, successCallback, errorCallback) {
        const xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState == XMLHttpRequest.DONE) {
                if (xhr.status == 200) {
                    if (typeof (successCallback) === "function") {
                        var result = '';

                        try {
                            result = JSON.parse(xhr.responseText);
                        }
                        catch (e) {
                            result = xhr.responseText;
                        }

                        successCallback(result, xhr.status, xhr);
                    }
                }
                else {
                    if (typeof (errorCallback) === "function")
                        errorCallback(xhr.responseText, xhr.status, xhr);
                }
            }
        };

        xhr.open(method, url, true);
        xhr.setRequestHeader('Content-Type', 'application/json');

        return xhr;
    }
    static get (url) {
        return new Promise((resolve, reject) => {
            var req = ajax.request(url, "GET", resolve, reject);
            req.send();
        });
    }
    static post (url, data) {
        return new Promise((resolve, reject) => {
            var req = ajax.request(url, "POST", resolve, reject);
            req.send(JSON.stringify(data));
        });
    }
    static put (url, data) {
        return new Promise((resolve, reject) => {
            var req = ajax.request(url, "PUT", resolve, reject);
            req.send(JSON.stringify(data));
        });
    }
    static delete (url, data) {
        return new Promise((resolve, reject) => {
            var req = ajax.request(url, "DELETE", resolve, reject);
            req.send(JSON.stringify(data));
        });
    }
};

