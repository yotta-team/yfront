const gulp = require('gulp'),
    watch = require('gulp-watch'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    cleanCSS = require('gulp-clean-css'),
    rename = require('gulp-rename'),
    htmlreplace = require('gulp-html-replace'),
    babel = require('gulp-babel'),
    jeditor = require('gulp-json-editor'),
    browserSync = require('browser-sync').create(),
    exec = require('child_process').exec;


// Task to publish
gulp.task('publish', ['npm-publish']);

gulp.task('update-version', function (cb) {
    return gulp.src('package.json')
        .pipe(jeditor(function (json) {
            var nums = json.version.split('.').map(n => +n);
            nums[nums.length - 1]++;
            json.version = nums.join('.');
            return json;
        }))
        .pipe(gulp.dest('./'));
});


gulp.task('npm-publish', ['update-version'], function (cb) {
    gulp.src('package.json')
        .pipe(jeditor(function (json) {
            exec(`git commit -am "updating package version" && npm publish`, function (err, stdout, stderr) {
                console.log(stdout);
                console.log(stderr);
                cb(err);
            });
            return json;
        }));
});